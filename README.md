# adjacency-net-v2

[![docs](https://img.shields.io/badge/docs-v0.1-blue.svg)](https://datapkg.gitlab.io/adjacency-net-v2/SWKhaBJiG8O53MTcw97h1vRjVgimQCb4mzTIH4H8dD10vrb6NWOUantGWhqHMoY7)
[![build status](https://gitlab.com/datapkg/adjacency-net-v2/badges/master/build.svg)](https://gitlab.com/datapkg/adjacency-net-v2/commits/master/)

Train and validate the Protein Adjacency Graph Neural Network (PAGNN).

## Notebooks
